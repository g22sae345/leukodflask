#! /usr/bin/python
# -- coding:utf-8 --
from flask import Blueprint
from flask import Flask, request, render_template, redirect, abort, flash, session

from connexion_db import get_db

client_article = Blueprint('client_article', __name__, template_folder='templates')

@client_article.route('/client/index')
@client_article.route('/client/article/show')              # remplace /client
def client_article_show():                                 # remplace client_index
    mycursor = get_db().cursor()
    id_client = session['id_user']
    list_param = []
    condition_and = ""
    # utilisation du filtre
    sql3=''' prise en compte des commentaires et des notes dans le SQL    '''
    articles =[]


    # pour le filtre
    sql = "SELECT libelle_type_parfum FROM type_parfum"
    mycursor.execute(sql)
    type_parfum = mycursor.fetchall()


    articles_panier = []

    sql = '''
            SELECT id_parfum AS id_article
                   , nom_parfum AS nom
                   , prix_parfum AS prix
                   , stock AS stock
                   , image AS image
            FROM parfum
            ORDER BY nom_parfum;
            '''
    mycursor.execute(sql)
    parfum = mycursor.fetchall()
    articles = parfum

    sql = '''
                SELECT id_volume  AS id_type_article
                        ,libelle_volume
                FROM volume
                ORDER BY  libelle_volume
                '''
    mycursor.execute(sql)
    volume = mycursor.fetchall()
    types_article = volume

    sql = "SELECT * , 10 as prix FROM ligne_panier" \
          "INNER JOIN parfum ON nom = parfum.nom_parfum"
#    sql = "SELECT parfum.nom_parfum AS nom, parfum.prix_parfum AS prix, ligne_panier.quantite AS quantite" \
 #          "ROUND((parfum.pix_parfum * ligne_panier.quantite),2)AS sous_total, declinaison.stock" \
  #         "FROM ligne_panier" \
   #        "INNER JOIN parfum ON ligne_panier.parfum_id = parfum.id_parfum" \
    #       "INNER JOIN decinaison ON parfum.id_parfum = declinaison.id_parfum" \
     #      "WHERE utilisateur_id = %s"

    mycursor.execute(sql)
    articles_panier = mycursor.fetchall()
    prix_total = 123  # requete à faire

    if len(articles_panier) >= 1:
        sql = ''' calcul du prix total du panier '''
        prix_total = None
    else:
        prix_total = None
    return render_template('client/boutique/panier_article.html'
                           , articles=articles
                           , articles_panier=articles_panier
                           #, prix_total=prix_total
                           , items_filtre=types_article
                           )