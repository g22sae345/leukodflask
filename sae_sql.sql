DROP TABLE IF EXISTS utilisateur, etat, commande, fournisseur, marque_parfum,
type_parfum, volume, parfum, ligne_commande, ligne_panier;

CREATE TABLE utilisateur(
    id_utilisateur INT AUTO_INCREMENT,
    login          VARCHAR(255),
    email          VARCHAR(255),
    nom            VARCHAR(255),
    password       VARCHAR(255),
    role           VARCHAR(255),

    est_actif      tinyint(1),
    PRIMARY KEY (id_utilisateur)
);
INSERT INTO utilisateur(id_utilisateur,login,email,password,role,nom,est_actif) VALUES
(1,'admin','admin@admin.fr',
    'sha256$dPL3oH9ug1wjJqva$2b341da75a4257607c841eb0dbbacb76e780f4015f0499bb1a164de2a893fdbf',
    'ROLE_admin','admin','1'),
(2,'client','client@client.fr',
    'sha256$1GAmexw1DkXqlTKK$31d359e9adeea1154f24491edaa55000ee248f290b49b7420ced542c1bf4cf7d',
    'ROLE_client','client','1'),
(3,'client2','client2@client2.fr',
    'sha256$MjhdGuDELhI82lKY$2161be4a68a9f236a27781a7f981a531d11fdc50e4112d912a7754de2dfa0422',
    'ROLE_client','client2','1');


CREATE TABLE etat(
    id_etat INT PRIMARY KEY,
    libelle_etat VARCHAR(255)
);
INSERT INTO etat(id_etat, libelle_etat) VALUES
(1, 'en attente'),
(2, ' expédié'),
(3, 'validé'),
(4, 'confirmé');


CREATE TABLE commande(
    id_commande INT PRIMARY KEY AUTO_INCREMENT,
    date_achat DATE,
    utilisateur_id INT ,
    etat_id INT,
     FOREIGN KEY (utilisateur_id) REFERENCES utilisateur(id_utilisateur),
  FOREIGN KEY(etat_id) REFERENCES etat(id_etat)
);
INSERT INTO commande(id_commande, date_achat, utilisateur_id, etat_id) VALUES
    (NULL, '20100815', 1, 1),
    (NULL, '20080513', 2, 3);


CREATE TABLE fournisseur(
    id_fournisseur INT PRIMARY KEY AUTO_INCREMENT,
    libelle_fournisseur VARCHAR(255)
);
INSERT INTO fournisseur(id_fournisseur, libelle_fournisseur) VALUES
(NULL, 'BALKAN LUXURY SUPPLY'),
(NULL, 'VINCE COMPANY PARFUMERIE'),
(NULL, 'ARTECAD'),
(NULL, 'NOVAC BEAUTY AND COSMETICS'),
(NULL, 'MELINADESTOCK'),
(NULL, 'GROSSISTE ORIENT'),
(NULL, 'OSMOZ');


CREATE TABLE marque_parfum(
    id_marque INT,
    libelle_marque VARCHAR(255),
    PRIMARY KEY (id_marque,libelle_marque)
);
INSERT INTO marque_parfum(id_marque, libelle_marque) VALUES
(1, 'KENZO'),
(2, 'AZZARO'),
(3, 'LOLITA LEMPICKA'),
(4, 'GUERLAIN'),
(5, 'HERMES'),
(6, 'NINA RICCI'),
(7, 'LEGEND'),
(8, 'FRESH');


CREATE TABLE type_parfum(
    id_type_parfum INT PRIMARY KEY AUTO_INCREMENT,
    libelle_type_parfum VARCHAR(255)
);
 INSERT INTO type_parfum  (id_type_parfum,libelle_type_parfum) VALUES
    (NULL, 'Homme'),
    (NULL, 'Femme'),
    (NULL, 'Enfant'),
    (NULL, 'mixte'),
    (NULL, 'pas cher'),
    (NULL, 'Cheveux'),
    (NULL, 'Maison'),
    (NULL, 'Luxe');


CREATE TABLE volume(
    id_volume INT PRIMARY KEY AUTO_INCREMENT,
    libelle_volume VARCHAR(255)
);
INSERT INTO volume(id_volume, libelle_volume) VALUES
(1, '50 ml'),
(2, '100 ml');


CREATE TABLE parfum(
    id_parfum INT PRIMARY KEY AUTO_INCREMENT,
    nom_parfum VARCHAR(255),
    prix_parfum NUMERIC(7,2),
    image VARCHAR(255) default 'no_photo.jpg',
    stock INT,
    marque_id INT,
    fournisseur_id INT,
    type_parfum_id INT,
    volume_id INT,
    FOREIGN KEY (marque_id) REFERENCES marque_parfum(id_marque),
    FOREIGN KEY (fournisseur_id) REFERENCES fournisseur(id_fournisseur),
    FOREIGN KEY (type_parfum_id) REFERENCES type_parfum(id_type_parfum),
    FOREIGN KEY (volume_id) REFERENCES volume(id_volume)
);
INSERT INTO parfum(id_parfum, nom_parfum, prix_parfum, image, stock, marque_id, fournisseur_id, type_parfum_id, volume_id) VALUES
(NULL, 'Flower by kenzo', 56.98, 'flower-by-kenzo-eau-de-parfum-15-ml.jpg',45, 1, 1, 2, 1),
(NULL, 'Flower by kenzo',62.15,'flower-by-kenzo-eau-de-parfum-30-ml.jpg', 70, 1, 1, 2, 2),
(NULL, 'Chrome', 38.84, 'chrome.jpg',50, 2, 2, 1, 1),
(NULL, 'Chrome', 46.86, 'chrome_100_ml.jpg', 40, 2, 2, 1, 2),
(NULL, 'Mon premier parfum',37.93, 'mon_premier_parf_30ml.jpg', 53, 3, 2, 3, 1),
(NULL, 'Mon premier parfum', 49.68, 'mon_premier_parf_100ml.jpg', 47, 3, 2, 3, 2),
(NULL, 'Santal Royal', 111.84, 'Santal_Royal_100ml.jpg', 14, 4, 6, 4, 1),
(NULL, 'Le jardin de monsieur Li', 43.40, 'Le_jardin_de_monsieur_Li_100ml.jpg',50, 5, 3, 4, 2),
(NULL, 'Nina', 38.84, 'nina_100ml.jpg',57, 6, 5, 5, 1),
(NULL, 'Montblanc', 24.84, 'Montblanc_100ml.jpg',60, 7, 4, 5, 2),
(NULL, 'Cannabis Santal', 100.00, 'Cannabis_Santal_100ml.jpg',40, 8, 7, 4, 2),
(NULL, 'Parfumdo', 28.84, 'Parfumdo_100ml.jpg',57, 3, 5, 6, 1),
(NULL, 'Cataliseur', 63.84, 'Cataliseur_100ml.jpg',8, 5, 2, 7, 2),
(NULL, 'Taticardie', 49.84, 'shopping.png',25, 7, 6, 7, 1),
(NULL, 'Salamie', 53.84, 'sala.png', 54, 2, 2, 8, 2);


CREATE TABLE ligne_commande(
    prix NUMERIC(7,2),
    quantite INT,
    commande_id INT,
    parfum_id INT,
    PRIMARY KEY (commande_id,parfum_id),
    FOREIGN KEY(commande_id) REFERENCES commande(id_commande),
    FOREIGN KEY(parfum_id) REFERENCES parfum(id_parfum)
);

CREATE TABLE ligne_panier(
    quantite INT,
    date_ajout DATETIME,
    utilisateur_id INT,
    parfum_id INT,
    PRIMARY KEY (utilisateur_id, parfum_id),
    FOREIGN KEY(utilisateur_id) REFERENCES utilisateur(id_utilisateur),
    FOREIGN KEY(parfum_id) REFERENCES parfum(id_parfum)
);

